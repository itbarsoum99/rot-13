# ROT13 Cipher Break

### Usage

```bash
git clone https://gitlab.com/itbarsoum99/rot-13.git

./break
./make
```

`break` unencodes files while `make` encodes and unencodes phrases passed as arguments.

*Created by Isaac Barsoum*
