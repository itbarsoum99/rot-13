#!/bin/bash

if [[ $# -ge 1 ]]; then
  echo $@|tr a-zA-Z n-za-mN-ZA-M
fi
while read line; do
  echo $line|tr a-zA-Z n-za-mN-ZA-M
done

<<former
if [[ -z $1 ]]; then
  echo "Usage: ./break filename"
else
  
  cat $1|tr a-zA-Z n-za-mN-ZA-M
fi
former
